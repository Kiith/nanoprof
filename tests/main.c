//          Copyright Ferdinand Majerech 2020.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include "nanoprof.h"

#include <stdio.h>

/// Timepoint IDs used in the test.
enum TIMEPOINT_ID
{
	TIMEPOINT_MAIN = 0,
	TIMEPOINT_MEASURE_TIMEPOINT,
	TIMEPOINT_INNER_TIMEPOINT
};

/// U32 data event IDs used in the test.
enum DATA_ID_U32
{
	DATA_COUNTER = 0,
};

/// Run a tight loop measuring every iteration as well as total loop time.
void measure_timepoint( struct nanoprof* prof )
{
	nanoprof_event_timepoint( prof, TIMEPOINT_MEASURE_TIMEPOINT );
	for( uint32_t i = 0; i < (1024 << 4) ; ++i )
	{
		// anecdata: 
		// takes ~6ns with -Os on Core i5-744HQ @ 2.80 GHz (Kaby Lake)
		// takes ~6ns with -O3 on Core i5-744HQ @ 2.80 GHz (Kaby Lake)
		// takes ~150ns with -Os on Cortex-A53 @ 1.2 GHz (RPi 3)
		// takes ~52ns with -O3 on Cortex-A53 @ 1.2 GHz (RPi 3)
		nanoprof_event_timepoint( prof, TIMEPOINT_INNER_TIMEPOINT );
		// don't spam a data event for each timepoint
		if( i % 10 == 0 )
		{
			nanoprof_event_data_u32( prof, DATA_COUNTER, i );
		}
		// no 'end' event here
	}
	nanoprof_event_timepoint_end( prof, TIMEPOINT_MEASURE_TIMEPOINT );
}

int main()
{
	FILE* file_dump = fopen("data.0.nanoprof", "w");

	const size_t buffer_size = 1024 << 1;
	// const size_t buffer_size = 1024 << 10;

	// This will create some 'return_buf' timepoints if buffer size is small
	// enough, due to filling the buffer with builtin ID register events and
	// then dumping it.
	struct nanoprof prof = nanoprof_create( buffer_size, file_dump );

	// Register all used timepoint IDs.
	nanoprof_event_register( &prof, TIMEPOINT_MAIN, 0, "main" );
	nanoprof_event_register( &prof, TIMEPOINT_MEASURE_TIMEPOINT, 1, "measure_timepoint" );
	nanoprof_event_register( &prof, TIMEPOINT_INNER_TIMEPOINT, 2, "inner_timepoint" );

	// Register the counter data event we record in the inner loop.
	nanoprof_event_register_data_u32( &prof, DATA_COUNTER, "counter");

	// Do actual profiling here.
	nanoprof_event_timepoint( &prof, TIMEPOINT_MAIN );
	measure_timepoint( &prof );
	nanoprof_event_timepoint_end( &prof, TIMEPOINT_MAIN );

	// Clean up.
	nanoprof_destroy( &prof );
	fclose( file_dump );
}
