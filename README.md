nanoprof
========

Minimalist real-time instrumenting tracing profiler library for C/C++.

Features
--------

* Implemented as a single C header depending only on the C standard library
* Written in C11 for compatibility with both C and C++
* *Tracing* profiler: records each instance of profiled events to view a timeline of process execution
* Can record arbitrary data for statistics, debugging, etc.
* Manual instrumentation
* Records user-specified variables
* Output can be viewed by a (TUI frontend)[https://codeberg.org/Kiith/timedilator-2] or Chromium `about://tracing`
* Low memory overhead (can go as low as 2B per recorded event)
* Low runtime overhead (measures time only once per scope when possible)
* Time precision on the order of 10ns
* Works on GCC/Clang on 32/64-bit x86 and 64-bit ARM
* Mimimalist, well documented code
* Memory allocation and output can be controlled by the user
* Faster than alternatives at the cost of a less user-friendly interface
  (need to specify an event's name/nest level before using it)
* No hidden, global state (but user must manage per-thread profiler contexts)

Planned features
----------------
* More compiler and platform support
* Real-time profiling over network


Getting Started
---------------

1. `git clone git@codeberg.org:Kiith/nanoprof.git`
2. Copy file `nanoprof/nanoprof.h` into your project or add its containing
   directory as an include path.
3. Add instrumentation to your program. Example:

```c
#include <nanoprof.h>

/// Timepoint IDs used.
enum TIMEPOINT_ID
{
    TIMEPOINT_PROGRAM = 0,
    TIMEPOINT_ACTIVITY,
    TIMEPOINT_OTHER_ACTIVITY
};

int main()
{
    FILE* file_prof = fopen("output.nanoprof", "w");
    if( !file_dump )
    {
        return 1;
    }

    const size_t buffer_size = 1024 * 1024;
    // Use `nanoprof_create_advanced` to gain full control over allocation and
    // to support different than FILE* output
    struct nanoprof prof = nanoprof_create( buffer_size, file_prof );

    // Register profiled events' ID's, names and nest levels.
    // Unwieldy but necessary to minimize amout of recorded data.
    // Events at lower nest levels end scopes of events at higher or equal nest levels.
    // Parameters: struct nanoprof, ID, scope nest level, name
    nanoprof_event_register( &prof, TIMEPOINT_PROGRAM,        0, "program" );
    nanoprof_event_register( &prof, TIMEPOINT_ACTIVITY,       1, "profiled_activity" );
    nanoprof_event_register( &prof, TIMEPOINT_OTHER_ACTIVITY, 1, "other_profiled_activity" );

    // Starts a level 0 scope for TIMEPOINT_PROGRAM.
    nanoprof_event_timepoint( &prof, TIMEPOINT_PROGRAM );
    for( size_t i = 0; i < 42; ++i )
    {
        // Starts a level 1 scope for TIMEPOINT_ACTIVITY.
        // Does *not* end scope of TIMEPOINT_PROGRAM since the level is higher.
        nanoprof_event_timepoint( &prof, TIMEPOINT_ACTIVITY );
        profiled_activity();
        // Ends the scope of TIMEPOINT_ACTIVITY and starts one for TIMEPOINT_OTHER_ACTIVITY
        nanoprof_event_timepoint( &prof, TIMEPOINT_OTHER_ACTIVITY );
        other_profiled_activity();
    }
    // Ends the scopes of TIMEPOINT_PROGRAM *and* the last TIMEPOINT_OTHER_ACTIVITY
    nanoprof_event_timepoint_end( &prof, TIMEPOINT_PROGRAM );

    // Clean up.
    nanoprof_destroy( &prof );
    fclose( file_dump );
}
```

4. Compile and run your program. Check that the profiling output (path
   specified in the `fopen()` call in above example) was created.

**Viewing using recorded data using TUI**

5. [Install Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html). You may also
   be able to install it from your system's repositories, e.g. `sudo apt install cargo` on
   Debian/Ubuntu and derivatives.

6. Make sure your cargo binary directory is in your `$PATH`. On Linux, this is usually 
   `~/.cargo/bin`.

7. Install timedilator-2

   `cargo install timedilator-2`

8. Use `timedilator-2` to view the profiling output:

   `timedilator-2 view output.nanoprof`

   This should open a TUI viewing specified stream, with usage information directly on the screen.

   Note: for multiple `.nanoprof` files from multiple threads, just pass all these files to
   `timedilator-2 view`


**Viewing using recorded data using Chromium (only fast enough for short profiles)**

5. Install Chromium for your platform.

6. Convert the output for Chromium's `about://tracing` (custom viewer is WIP)
   using the utility script `nanoprof2chrome.d`

   `chmod +x ./util/nanoprof2chrome.d`

   `cat output.nanoprof | ./util/nanoprof2chrome.d > output.json`

7. Open ``about://tracing`` in Chromium and open file `output.json`.
   This will take forever for non-trivial outputs - that's why we need
   a custom viewer. Also note that since ``about://tracing`` cannot handle
   nanoseconds, all times are multiplied by 1000 (e.g. a millisecond in
   ``about://tracing`` output is a microsecond in program run time).

See the [API reference](https://codeberg.org/Kiith/nanoprof/raw/branch/master/doc/nanoprof.pdf)
for more information.

The reference is in PDF format, you can also generate HTML yourself with doxygen:

```
doxygen Doxyfile
```

which will generate documentation in the `doc/html` directory.

Tracing profilers
-----------------

Most profilers show a **statistical overview** on where most time is spent. This can be done by
counting calls of a function, repeatedly interrupting a process to check current instruction, etc.
The result is usually a statistical breakdown of **X% of time is spent in function Y**, sometimes
with a call graph.

**Tracing profilers** like `nanoprof` record *every instance* of profiled code separately; every
function call, loop iteration, or anything else you need to measure. This is generally leads to more
overhead and *much* more memory usage than a traditional profiler, but allows much more detailed 
observation of the program.

A traditional profiler can show that `std::sort` is taking up 30% of time. A tracing profiler will
show that `std::sort` is usually very fast, but its 24th call takes forever, and may even be able to
record parameters that caused this to happen.

Unlike most profilers, a tracing profiler cannot afford to record every single function, requiring
the user to manually instrument the code. In case of `nanoprof` this is done with **timepoint**
events.

Design philosophy
-----------------

Most tracing profilers, like `optick`, `MicroProfiler` or `remotery`, rely on the user to insert
function calls or C++ RAII classes to record *events* marking the beginning and end of profiled
activity, with a name. When the activity is entered/exited, a timestamp is recorded together with
a name.

This is easy to use as it the user can insert a function call or macro at any place, without much
initialization / setup. For profiling games or other applications working with millisecond-level
latencies, this is usually the best approach and such tools will be a better fit than `nanoprof`.

`nanoprof` is targeted at profiling multi-gigabit packet processing code, which often works with
microsecond or multi-nanosecond latencies. We sacrifice user friendliness for **minimum overhead
achievable**.

`nanoprof` achieves high performance thanks to the following observarions:

1. *time measurement is expensive*: Commonly-used high-precision timers (such as those in the C++
   `std::chrono`) have high overhead, which can skew profiling results.
2. *memory is expensive*: Recording a 64bit timestamp, scope depth and a string for each event
   quickly eats through memory, uses up memory bandwidth and eventually leads to I/O overhead which
   further skews the results.
3. *tracing profilers sometimes do double the work*: Usually both the beginning and the end of a
   scope are recorded. But in tight loops (which record most events), *end* of an iteration is
   immediately followed by the *beginning* of the next. It is possible to measure time only once in
   this case.

These lead to the following implementation decisions:

1. `nanoprof` uses fast, but less reliable ways to measure time, e.g. the `rdtsc` instruction on
   x86/AMD64. This skews the results due to imprecision, but less so than the overhead of standard
   timers.  This is not uncommon - most of tracing profilers use such fast timing instructions.
2. `nanoprof` requires the user to *register* profiled events, associating a *timepoint* ID (event
   ID) with a name *and* nest level. To record an event, only this ID has to be written, which uses
   1 or 2 bytes depending on ID value.

   `nanoprof` records time in *deltas*, and uses less bytes if the delta value is small enough (1, 2
   or 6 bytes can be used).

   In dense loops, `nanoprof` only writes 2 bytes per event (the denser a loop, the smaller the
   deltas).
3. `nanoprof` doesn't internally think in *scopes* - those are generated when post-processing
   recorded data. In a loop, a *timepoint* event only has to be added at the beginning of an
   iteration. This will result in a scope ending at the beginning of the next iteration, or at the
   first event after the loop. Special 'end' event can be recorded immediately after a loop or any
   other activity we need to explicitly end.

   Events registered with lower nest levels end scopes for events with higher or equal nest levels. 

For more details, see:

* `FORMAT.md` for the (unstable!) nanoprof event stream format specification.
* `nanoprof/nanoprof.h` for nanoprof implementation
* `util/nanoprof2chrome.d` for how nanoprof events can be turned into scopes
  viewable by Chrome `about://tracing`.

License
-------

```
         Copyright Ferdinand Majerech 2020-2022.
Distributed under the Boost Software License, Version 1.0.
   (See accompanying file LICENSE_1_0.txt or copy at
         http://www.boost.org/LICENSE_1_0.txt)
```

Similar Projects
----------------

* [MicroProfiler](https://github.com/tyoma/micro-profiler)
* [C++ Rapid Profile](https://github.com/grant-zietsman/rapid-profile)
* [Remotery](https://github.com/Celtoys/Remotery)
* [easy_profiler](https://github.com/yse/easy_profiler)
* [optick](https://github.com/bombomby/optick)
* [Telemetry (closed source)](http://www.radgametools.com/telemetry.htm)
