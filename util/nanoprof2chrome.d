#!/snap/bin/rdmd -release

//          Copyright Ferdinand Majerech 2020.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

import std.array;
import std.stdio;
import std.typecons;

string help =
"
Usage: cat INPUT | nanoprof2chrome.d > OUTPUT.json

  $ cat test.nanoprof | ./nanoprof2chrome.d > test.json

nanoprof to chromium trace format converter.
Open output in chrome://tracing in Chromium/Chrome.

Note: nanoprof measures time in nanoseconds, but chromium
cannot handle nanoseconds reliably, so we represent a
nanosecond as a microsecond in generated trace file.
";

/// Nanoprof event types.
enum EventType: uint
{
	/// Time point: time delta from last time point + event ID.
	Timepoint   = 0,
	/// High-precision timestamp. Used to correct time point values.
	HighPrec    = 0xFD,
	/// Data event: data type followed by an integer/float/string.
	Data        = 0xA,
	/// Timepoint event ID registration. Assigns a level and name to an ID.
	IDRegister  = 0xFE,
	/// Padding byte.
	Padding     = 0xFF,
	/// Unknown/error case.
	Unknown     = 0xFFFFFFFE,
}

/** Determine type of the current nanoprof event.
 *
 * Event type can always be determined from the first byte of an event.
 * 
 * Event type is determined by a 'prefix' - the first byte of any event of
 * a given type must start with the prefix, but different types may have
 * prefixes of different lengths. Remaining after the prefix, if any, are
 * used for event data. More common events have shorter prefixes so they can
 * use more bits for the event itself.
 *
 * Event type prefixes:
 * * 0b0:        timepoint (most common event)
 * * 0b1010:     data
 * * 0b11111101: highprec
 * * 0b11111110: timepoint event ID register
 * * 0b11111111: padding byte
 */
EventType toEventType( const ubyte firstByte )
{
	if( (firstByte >> 7) == 0b0 )    { return EventType.Timepoint; }
	if( (firstByte >> 4) == 0b1010 ) { return EventType.Data; }
	switch( firstByte )
	{
		case 0b11111101: return EventType.HighPrec;
		case 0b11111110: return EventType.IDRegister;
		case 0b11111111: return EventType.Padding;
		default: break;
	}
	stderr.writefln( "Unknown event type: 0x%x", firstByte );
	return EventType.Unknown;
}

/** Timepoint event data extracted from nanoprof stream.
 * 
 * The timepoint event only contains an ID + time delta in the stream;
 * that ID and time delta is then used to calculate name, level and 
 * exact time - after the event is read.
 *
 * Every timepoint starts a 'scope' that ends at the next timepoint on the same
 * level. This allows to measure time taken by a loop body with only one time
 * measurement (at the beginning of the loop body) instead of two - a 'loop body
 * scope' starts at the beginning of one iteration and ends at the beginning of
 * another.
 *
 * Special reserved 'scope end' timepoint IDs can be use to end the last scope at
 * a given event without starting a new scope - in above case, we'd record such
 * an 'end event' right after exiting the loop.
 */
struct TimepointEvent
{
	// Taken from nanoprof.h
	/// Max value of a user-specified timepoint event ID.
	enum NANOPROF_TIMEPOINT_ID_MAX_USER = 0x3BFF;
	/// Value of timepoint 'scope end' event for level -128
	enum NANOPROF_TIMEPOINT_ID_MIN_END  = (NANOPROF_TIMEPOINT_ID_MAX_USER + 1);
	/// Value of timepoint 'scope end' event for level 127
	enum NANOPROF_TIMEPOINT_ID_MAX_END  = (NANOPROF_TIMEPOINT_ID_MIN_END + 255);

	/** Time delta from the last timepoint event, in 'time units'.
	 *
	 * Length of a 'time unit' depends on the time measurement method. Time
	 * units can be converted to nanoseconds by determining the delay between
	 * surrounding HighPrecEvents in both 'time units' and nanoseconds, and
	 * using the 'time unit' delay from the last HighPrecEvent to the
	 * TimepointEvent.
	 */
	ulong timeDelta;
	/** ID of the timepoint event.
	 *
	 * Currently, timepoint event IDs are limited to 14-bit values.
	 */
	uint  id;

	/** Name of the event.
	 *
	 * Determined by matching `id` with same ID in a previous IDRegisterEvent.
	 */
	string name;
	/** Nesting level of the event.
	 *
	 * Determined by matching `id` with same ID in a previous IDRegisterEvent.
	 */
	byte  level;
	/** Exact time value in nanoseconds, calculated when we get the next
	 * HighPrecEvent after the TimepointEvent.
	 */
	ulong nsecs;

	/// True if this is an 'end' event and does not start a scope.
	bool isEnd() const
	{
		return id >= NANOPROF_TIMEPOINT_ID_MIN_END && id <= NANOPROF_TIMEPOINT_ID_MAX_END;
	}
}

/** High-precision timestamp event.
 *
 * Uses high-precision clock to measure time in nanoseconds, which is used to
 * set the 'exact time' (nsecs) value of immediately following TimepointEvent.
 */
struct HighPrecEvent
{
	/// Exact time in nanoseconds.
	ulong nsecs;
}

/// A data event, recording a tracing value/stat of one of various data types.
struct DataEvent
{
	import std.variant;
	// ID of the data event.
	uint id;
	Algebraic!(
		ubyte,
		ushort,
		uint,
		ulong,
		float,
		double,
		string) data;
}

/// Types of event IDs used in nanoprof.
enum IDType: ubyte
{
    Timepoint  = 0b0000_0001,
    DataU8     = 0b0000_0010,
    DataU16    = 0b0000_0011,
    DataU32    = 0b0000_0100,
    DataU64    = 0b0000_0101,
    DataF32    = 0b0000_0110,
    DataF64    = 0b0000_0111,
    DataString = 0b0000_1000,
}

/** Event registering a timepoint event ID to give a level and name.
 *
 * The name is stored in a data event immediately after the ID register event.
 * This data event must be parsed to get the ID's name.
 */
struct IDRegisterEvent
{
	/// ID being registered.
	uint id;
	/// Nest level of the event.
	byte level;
	/// Type of ID being registered.
	IDType id_type;
	/// Name of the event.
	string name;
}

/** Reads nanoprof events from a file (which can be stdin).
 *
 * This is a rather low-level API, has to be driven by user-side loop that
 * checks the type of the next event and calls the correct function to read it;
 * this may be (TODO) refactored later.
 */
struct EventReader
{
public:
	/// Construct an EventReader reading specified file.
	this( File rhsFile )
	{
		file = rhsFile;
		// We have at most 2^14 event IDs.
		timepointIDs = new RegisteredTimepointID[1 << 14];
		buffer.reserve( BASE_BUFFER_SIZE );
	}

	/** Get the type of the next event, if any, in the stream.
	 *
	 * Returns:
	 * * EventType of the next event on success.
	 * * EventType.Unknown if there is an unknown/invalid event.
	 * * null if there is no event/we're at the end of the file.
	 */
	Nullable!EventType nextEventType()
	{
		if( ubyte[] bytes = peek( 1 ) )
		{
			return nullable(bytes[0].toEventType);
		}
		assert(file.eof && offset >= buffer.length,
			"returning no events but we sill havve data to read");
		return Nullable!EventType.init;
	}

	/** Register a timepoint event ID so we know level/name of timepoints with
	 * that ID.
	 *
	 * Params:
	 * idEvent = Event registering the ID (has event ID and level).
	 * name    = Name of the ID.
	 */
	void registerTimepointID( in IDRegisterEvent idEvent )
	{
		timepointIDs[idEvent.id] = RegisteredTimepointID( idEvent.name, idEvent.level );
	}

	/// Skip a padding byte; call iff nextEventType == EventType.Padding.
	void skipPadding()
	{
		get(1);
	}

	/** Read a timepoint event to `result`.
	 *
	 * A timepoint event can be in one of 3 formats described in FORMAT.md .
	 *
	 * Call iff `nextEventType == EventType.Timepoint`.
	 *
	 * Returns: true on success, false if we ran out of data.
	 */
	bool readEventTimepoint( out TimepointEvent result )
	{
		// If the event is in the 2B format, this stores the entire event.
		// For 4B/8B format, this stores the ID.
		const ubyte[] bytes2B = get( 2 );
		if( bytes2B.empty ) { return false; }
		// If 1st bit of 2nd byte is 0, that byte is a 1B timedelta and
		// we have the 2B format.
		const bool format2B = 0 == (bytes2B[1] & 0b1000_0000);

		scope(exit) with( timepointIDs[result.id] )
		{
			// If event not registered, handle it as 'unregistered'.
			result.level = isNull ? -127                           : level;
			result.name  = isNull ? "Unregistered timepoint event" : name;
		}

		// Simplest format: 7-bit ID with a 7-bit time delta, top bits 0.
		if( format2B )
		{
			result.id        = bytes2B[0];
			result.timeDelta = bytes2B[1];
			return true;
		}

		// If 1st bit of 3rd byte is 0, that byte is the start of a 2B
		// timedelta (4B format), otherwise the timedelta is 6B (8B format)
		const bool format4B = 0 == (peek(1)[0] & 0b1000_0000) ;
		// Compose a 14-bit ID from the 2 bytes (top bits indicate format).
		result.id = (bytes2B[0] << 7) | (bytes2B[1] & 0b0111_1111);
		if( format4B )
		{
			const ubyte[] bytesTime = get( 2 );
			result.timeDelta = (bytesTime[0] << 8) | bytesTime[1];
			return true;
		}

		const ubyte[] bytesTime = get( 6 );
		// Top bit of bytesTime[0] is 1 so we have to start by zeroing that.
		result.timeDelta = 
			((bytesTime[0] & 0b0111_1111UL) << 40) |
			(cast(ulong)(bytesTime[1]) << 32) |
			(cast(ulong)(bytesTime[2]) << 24) |
			(cast(ulong)(bytesTime[3]) << 16) |
			(cast(ulong)(bytesTime[4]) << 8) |
			 cast(ulong)(bytesTime[5]);

		return true;
	}

	/** Read a high-precision timestamp event.
	 *
	 * Records a precise timestamp in nanoseconds.
	 *
	 * Each highprec event is associated with the next timepoint event for which
	 * it sets the time value. Each pair of successive highprec events is used
	 * to compute time delta and clock rate between associated timepoint events.
	 *
	 * Format: see FORMAT.md
	 *
	 * Call iff `nextEventType == EventType.HighPrec`.
	 *
	 * Returns: true on success, false if we ran out of data.
	 */
	bool readEventHighPrec( out HighPrecEvent result )
	{
		if( ubyte[] bytes = get( 8 ) )
		{
			result.nsecs =
				(cast(ulong)(bytes[1]) << 48) |
				(cast(ulong)(bytes[2]) << 40) | (cast(ulong)(bytes[3]) << 32) |
				(cast(ulong)(bytes[4]) << 24) | (cast(ulong)(bytes[5]) << 16) |
				(cast(ulong)(bytes[6]) <<  8) |  cast(ulong)(bytes[7]);
			return true;
		}
		return false;
	}

	/** Read a data event, which stores a single value of various datatypes.
	 *
	 * Used to record interesting tracing data, which may later be e.g. graphed.
	 *
	 * Format: `1010_TTTT DDDD_DDDD ... DDDD_DDDD`
	 *
	 * TTTT is the data type. DDDD_DDDD ... DDDD_DDDD is a single fixed-size
	 * value of specified data type, in native byte order. E.g. if data type is
	 * uint8_t (0001), there is a single data byte.
	 *
	 * Data type string (1111) is a special case - its length is unknown, and
	 * is terminated by a zero byte - like a C string.
	 *
	 * Call iff `nextEventType == EventType.Data`.
	 *
	 * Returns: true on success, false if we ran out of data or on unknown
	 *          datatype.
	 */
	bool readEventData( out DataEvent result )
	{
		const ubyte[] type_and_id = get(3);
		const type = type_and_id[0] & 0xF;
		result.id  = ((type_and_id[1] & 0b00111111) << 8) | type_and_id[2]; 
		if( !type_and_id ) { return false; }
		// Read a simple value of specified type.
		bool readSimpleData(T)()
		{
			T[] data = cast(T[])get(T.sizeof);
			if( data ) { result.data = data[0]; }
			return data != null;
		}
		switch( type )
		{
			// unknown data type
			case 0x0: return false;
			case 0x1: return readSimpleData!ubyte;
			case 0x2: return readSimpleData!ushort;
			case 0x3: return readSimpleData!uint;
			case 0x4: return readSimpleData!ulong;
			case 0x5: return readSimpleData!float;
			case 0x6: return readSimpleData!double;
			// unknown data type
			case 0x7 - 0xE: return false;
			case 0xF:
				// Read until, not including, a zero terminator byte.
				if( ubyte[] data = getUntil( 0 ) )
				{
					result.data = (cast(char[])data).idup;
					//stderr.writefln( "data string: %s", result.data.get!string );
					// skip the zero terminator
					return get(1) == [0];
				}
				return false;
			default:
				assert( false, "Unreachable" );
		}
		return true;
	}

	/** Read a timepoint ID registering event.
	 *
	 * Specifies the nest level and name for a timepoint ID.
	 *
	 * The name is in an immediately following data event with the string type.
	 *
	 * Format: see FORMAT.md
	 *
	 * Call iff `nextEventType == EventType.IDRegister`.
	 *
	 * Returns: true on success, false if we ran out of data.
	 */
	bool readEventIDRegister( out IDRegisterEvent result )
	{
		if( ubyte[] bytes = get( 5 ) )
		{
			result.level   = cast(byte)bytes[1];
			result.id_type = cast(IDType)bytes[2];
			// 14-bit ID
			result.id      = ((bytes[3] & 0b00111111) << 8) | bytes[4];
			// Read until, not including, a zero terminator byte.
			if( ubyte[] data = getUntil( 0 ) )
			{
				result.name = (cast(char[])data).idup;
				// skip the zero terminator
				return get(1) == [0];
			}

			return false;
		}
		return false;
	}

private:
	/// Base size for the read buffer (the buffer may get larger).
	enum BASE_BUFFER_SIZE = 64 * 1024;

	/// Information about a registerd timepoint ID.
	struct RegisteredTimepointID
	{
		/// Name of the scope started by the timepoint.
		string name;
		/// Nesting/'callgraph' level of the timepoint.
		byte level;
		/// Returns true for an uninitialized timepoint ID.
		bool isNull() { return name == null; }
	}

	/** Look at next `size` bytes in the stream without consuming them.
	 *
	 * Returns:
	 * Slice of `size` bytes on success, empty slice if we run out of data.
	 */
	ubyte[] peek( const size_t size )
	{
		while( !file.eof && offset + size > buffer.length )
		{
			getData();
		}
		return ( offset + size <= buffer.length )
			? buffer[offset .. offset + size]
			: [];
	}

	/** Consume the next `size` bytes.
	 *
	 * Returns:
	 * Slice of `size` bytes on success, empty slice if we run out of data.
	 */
	ubyte[] get( const size_t size )
	{
		scope(exit) { offset += size; }
		return peek( size );
	}

	/** Consume all bytes until `terminator` is reached. `terminator` is also consumed.
	 *
	 * Used to read e.g. zero-terminated strings.
	 *
	 * Returns:
	 * * Variable-size slice of bytes ending right before `terminator`.
	 * * Empty slice if EOF is reached without encountering `terminator`.
	 */
	ubyte[] getUntil( const ubyte terminator )
	{
		// stderr.writeln("getUntil");
		import std.algorithm: countUntil;
		auto zeroIndex = buffer[offset .. $].countUntil(terminator);
		if( zeroIndex > 0 )
		{
			return get( zeroIndex );
		}
		auto processedBytes = buffer.length - offset;
		while( !file.eof )
		{
			getData();
			zeroIndex = buffer[offset + processedBytes .. $].countUntil(terminator);
			if( zeroIndex > 0 )
			{
				return get( processedBytes + zeroIndex );
			}
			// We've processed all bytes up to current buffer end.
			processedBytes = buffer.length - offset;
		}
		// EOF reached without finding terminator
		return [];
	}

	/** Read more data into the buffer and discard consumed data.
	 *
	 * Reads up to BASE_BUFFER_SIZE bytes, moves data in buffer so the next
	 * byte to consume is at byte 0, and resets offset to 0.
	 */
	void getData()
	{
		import std.algorithm: moveAll;
		// Throw away data we've done processing.
		buffer[offset .. $].moveAll( buffer[0 .. $ - offset] );

		buffer = buffer[0 .. $ - offset];
		offset = 0;
		ubyte[BASE_BUFFER_SIZE] readBuf;
		buffer ~= file.rawRead(readBuf);
	}

	/// File data buffer.
	ubyte[] buffer;

	/// Offset of the first unconsumed byte in `buffer`.
	size_t offset = 0;

	/// Input file.
	File file;

	/** Information about registered timepoint ID's, indexed by ID.
	 *
	 * (Used as a hash table - not all items are initialized)
	 */
	RegisteredTimepointID[] timepointIDs;
}

/** Program entry point.
 */
int main(string[] args)
{
	import std.algorithm: canFind;
	if( args.canFind( "--help" ) || args.canFind("-h") )
	{
		stderr.writeln( help );
		return 0;
	}

	// Every timepoint event not marked as an 'End' event starts a time 'scope'
	// that ends at the next timepoint event at same level. pendingScopes stores
	// timepoint events we haven't found the 'end' for yet.
	TimepointEvent[] pendingScopes;
	// Parsed timepoint events are buffered here until we reach a HighPrec event
	// - at that point the events are processed and the buffer is cleared.
	TimepointEvent[] timepointBuffer;
	// Previous high prec event (NULL before the first such event)
	Nullable!HighPrecEvent prevHighPrec;
	assert( prevHighPrec.isNull );
	// Output is a JSON array.
	writeln( "[" );
	scope( exit ) { writeln("]"); }
	// True before creating the first scope.
	bool firstScope = true;
	// Program start time is written here from the first HighPrec event.
	ulong programStartNSec = 0;

	// Events are serialialized this buffer before being written out.
	char[65536] eventBuf;

	/** Write a time scope to stdout as a JSON object.
	 *
	 * Params:
	 * start    = Start timepoint of the scope.
	 * nsecsEnd = End of the scope as absolute time in nanoseconds.
	 */
	void writeScope( in TimepointEvent start, const ulong nsecsEnd )
	{
		import std.format;
		auto event = sformat(
			eventBuf[],
			// TODO events for PID and TID
			q{{"cat":"nanoprof","pid":"TODO","tid":"TODO","ph":"X","name":"%s","ts":%u,"dur":%u}},
			start.name,
			start.nsecs - programStartNSec,
			nsecsEnd - start.nsecs );
		if( !firstScope ) { writeln( "," ); }
		firstScope = false;
		write( event );
	}

	/** Try to find the end point of a time scope, and if found, write it to output.
	 *
	 * Each scope ends at the next time point at the same or lower level.
	 *
	 * Params:
	 * start             = Start time point of the scope.
	 * possibleEndPoints = Buffer of timepoints (sorted by time) that may contain
	 *                     the end point of the scope.
	 *
	 * Returns: true if the scope has been closed and written, false otherwise.
	 */
	bool closeScope(
		in TimepointEvent   start,
		in TimepointEvent[] possibleEndPoints )
	{
		import std.algorithm: find;
		// Scope can end at a timepoint at same or lower level, except level -128
		// which is separate and used for nanoprof self-profiling.
		auto scopeEnd = possibleEndPoints.find!(
			// Scopes at level -128 can only be ended at the same level
			(a) => start.level == -128 ? a.level == -128 :
			// Scopes at level higher than -128 can be ended at any lower level other than -128
			       (a.level != -128 && a.level <= start.level) )();
		// Scope end not found in possibleEndPoints.
		if( scopeEnd.empty ) { return false; }
		writeScope( start, scopeEnd.front.nsecs );
		// Re-add this if needed: Subtract 1 if this is ended by a parent scope,
		// to make sure Chrome about://tracing renders a child scope as a child.
		// writeScope( start, scopeEnd.front.level == start.level ? scopeEnd.front.nsecs : scopeEnd.front.nsecs - 1 );
		return true;
	}

	/** Handle a HighPrec event.
	 *
	 * Determine exact time values for all timepoint events since the last
	 * HighPrec event, and try to close scopes started by those Timepoint
	 * events. Closed scopes are written to output.
	 *
	 * Params:
	 * newHighPrec = current HighPrec event.
	 */
	void handleHighPrec( const HighPrecEvent newHighPrec )
	{
		// If this is the first highprec event, don't process any timepoints as we don't
		// have any 'reference'
		if( prevHighPrec.isNull )
		{
			programStartNSec = newHighPrec.nsecs;
			prevHighPrec = newHighPrec;
			return;
		}

		// Reset the timepoint buffer and previous highprec event when done.
		scope(exit)
		{
			timepointBuffer = timepointBuffer[0 .. 0];
			prevHighPrec = newHighPrec;
		}
		// May happen when input has a lot of metadata.
		if( timepointBuffer.empty )
		{
			return;
		}
		import std.algorithm: sum, map, cumulativeFold;
		// Time between the highprec events.
		// Avoiding precision issues by counting time in picoseconds
		const ulong totalPSecs = 1000 * (newHighPrec.nsecs - prevHighPrec.get.nsecs);

		// Total 'time units' between the HighPrec events.
		const ulong totalDelta = timepointBuffer.map!( p => p.timeDelta ).sum;
		// stderr.writeln( "totalDelta: ", totalDelta );
		// One 'time unit' of the time delta
		const ulong timeUnitPSecs = totalPSecs / totalDelta;

		// Compute absolute time values of the individual events in timepointBuffer.
		ulong cumulativePSecs = 0;
		foreach( ref timepoint; timepointBuffer )
		{
			cumulativePSecs += timepoint.timeDelta * timeUnitPSecs;
			timepoint.nsecs = prevHighPrec.get.nsecs + (cumulativePSecs / 1000);
		}

		// For each pending timepoint, search for next timepoint on same level
		// and if found, write a scope to output, 'closing' it.
		size_t dst = 0;
		for( size_t src = 0; src < pendingScopes.length; ++src )
		{
			assert( !pendingScopes[src].isEnd, 
				"End events must not be added to pendingScopes" );
			// Events are ordered by their end time. This is not a problem for
			// Chrome, which sorts the events by timestamp.
			// Any pending scope has already been checked against scopes in
			// pendingScopes - we only need to check it against timepointBuffer.
			if( !closeScope(pendingScopes[src], timepointBuffer ) )
			{
				pendingScopes[dst] = pendingScopes[src];
				++dst;
			}
		}
		pendingScopes = pendingScopes[0 .. dst];

		// Close scopes started by timepoints in timepointBuffer.
		// When we reach an end event, it can be removed as all scopes it could
		// close are already closed.
		foreach( i, ref timepoint; timepointBuffer ) if( !timepoint.isEnd )
		{
			// Use pendingScopes to keep scopes we aren't able to close.
			if( !closeScope( timepoint, timepointBuffer[i + 1 .. $] ) )
			{
				pendingScopes ~= timepoint;
			}
		}
	}

	// A HighPrec event needs the next Timepoint event to know the full
	// extent of 'timepoint time' between 2 highprec events.
	// Note that the timepoint may not necessarily follow the highprec event
	// *immediately*.
	Nullable!HighPrecEvent pendingHighPrecEvent;

	// Read the events from stdin.
	auto reader = EventReader( stdin );
	while( true )
	{
		const type = reader.nextEventType;
		if( type.isNull ) { break; }
		final switch( type.get ) with( EventType )
		{
			case Timepoint:
				// We buffer timepoint events until we reach a highprec event.
				timepointBuffer ~= TimepointEvent();
				if( !reader.readEventTimepoint(timepointBuffer.back) )
				{
					stderr.writeln( "ERROR: Input ended in the middle of a Timepoint event" );
					return 1;
				}
				// A HighPrec event needs the following timepoint event before
				// being handled - that timepoint is assigned the HighPrec's
				// absolute time value.
				if( !pendingHighPrecEvent.isNull )
				{
					handleHighPrec( pendingHighPrecEvent.get() );
					destroy( pendingHighPrecEvent );
				}
				break;
			case HighPrec:
				HighPrecEvent highPrec;
				if( !reader.readEventHighPrec(highPrec) )
				{
					stderr.writeln( "ERROR: Input ended in the middle of a HighPrec event" );
					return 1;
				}
				// Need to get the following timepoint event before handling
				pendingHighPrecEvent = highPrec;
				break;
			case Data:
				// TODO chrome can graph data with the 'plot' feature - look at
				// chrome tracing doc & maybe cxxprof on how to handle it.
				DataEvent data;
				if( !reader.readEventData(data) )
				{
					stderr.writeln( "ERROR: Input ended in the middle of a data event" );
					return 1;
				}
				break;
			case IDRegister:
				IDRegisterEvent idRegister;
				if( !reader.readEventIDRegister(idRegister) )
				{
					stderr.writeln( "ERROR: Input ended in the middle of an ID register event" );
					return 1;
				}
				switch( idRegister.id_type )
				{
					case IDType.Timepoint:
						reader.registerTimepointID( idRegister );
						break;
					default:
						stderr.writefln("unknown id type %s", cast(ubyte)idRegister.id_type);
						// do nothing, we don't emit data events into chrome format (if it even
						// supports them)
				}
				break;
			case Padding:
				// Padding bytes written at nanoprof to align to buffer size.
				reader.skipPadding();
				break;
			case Unknown:
				stderr.writeln( "ERROR: Unknown event ID encountered" );
				return 2;
		}
	}

	if( !pendingHighPrecEvent.isNull )
	{
	    // This should not happen anymore.
	    stderr.writeln( "Unexpected HighPrec event at end of the buffer" );
	}

	// End scopes that had no event to end at (may happen due to any early cutoff).
	if( !prevHighPrec.isNull ) foreach( ref timepoint; pendingScopes )
	{
		writeScope( timepoint, prevHighPrec.get.nsecs );
	}
	return 0;
}
