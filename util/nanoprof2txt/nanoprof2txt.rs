/// Converts nanoprof stream into human-readable text data - for debugging of nanoprof output.
use std::env;
use std::io;
use std::fmt;
use std::io::Read;
use std::fs::File;
use std::slice;
use std::convert::TryInto;


/// Types of nanoprof events.
enum EventType {
    Timepoint,
    HighPrecisionTime,
    Data,
    RegisterTimepoint,
    Padding,
}

/// Types of event IDs used in nanoprof.
enum IDType {
    Timepoint,
    DataU8,
    DataU16,
    DataU32,
    DataU64,
    DataF32,
    DataF64,
    DataStr,
}

impl fmt::Display for IDType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            IDType::Timepoint => write!(f, "Timepoint" ),
            IDType::DataU8    => write!(f, "DataU8"    ),
            IDType::DataU16   => write!(f, "DataU16"   ),
            IDType::DataU32   => write!(f, "DataU32"   ),
            IDType::DataU64   => write!(f, "DataU64"   ),
            IDType::DataF32   => write!(f, "DataF32"   ),
            IDType::DataF64   => write!(f, "DataF64"   ),
            IDType::DataStr   => write!(f, "DataStr"   ),
        }
    }
}

/// Stores a nanoprof event of any type.
enum Event {
    Padding,
    Timepoint16
    {
        id: u8,
        time_delta: u8
    },
    Timepoint32
    {
        id: u16,
        time_delta: u16
    },
    Timepoint64
    {
        id: u16,
        time_delta: u64
    },
    HighPrecisionTime
    {
        current_time: u64
    },
    DataU8
    {
        id: u16,
        value: u8
    },
    DataU16
    {
        id: u16,
        value: u16
    },
    DataU32
    {
        id: u16,
        value: u32
    },
    DataU64
    {
        id: u16,
        value: u64
    },
    DataF32
    {
        id: u16,
        value: f32
    },
    DataF64
    {
        id: u16,
        value: f64
    },
    DataStr
    {
        id: u16,
        value: String
    },
    RegisterTimepoint
    {
        nest_level: i8,
        id_type:    IDType,
        id:         u16,
        name:       String
    }
}


impl fmt::Display for Event {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Event::Padding                                          => write!(f, "Padding"),
            Event::Timepoint16{id, time_delta}                      => write!(f, "Timepoint16({}, {})", id, time_delta ),
            Event::Timepoint32{id, time_delta}                      => write!(f, "Timepoint32({}, {})", id, time_delta ),
            Event::Timepoint64{id, time_delta}                      => write!(f, "Timepoint64({}, {})", id, time_delta ),
            Event::HighPrecisionTime{current_time}                  => write!(f, "HighPrecisionTime({})", current_time ),
            Event::DataU8{id, value}                                => write!(f, "DataU8({}, {})",  id, value ),
            Event::DataU16{id, value}                               => write!(f, "DataU16({}, {})", id, value ),
            Event::DataU32{id, value}                               => write!(f, "DataU32({}, {})", id, value ),
            Event::DataU64{id, value}                               => write!(f, "DataU64({}, {})", id, value ),
            Event::DataF32{id, value}                               => write!(f, "DataF32({}, {})", id, value ),
            Event::DataF64{id, value}                               => write!(f, "DataF64({}, {})", id, value ),
            Event::DataStr{id, value}                               => write!(f, "DataStr({}, {})", id, value ),
            Event::RegisterTimepoint{nest_level, id_type, id, name} => write!(f, "RegisterTimepoint({}, {}, {}, {})", id_type, nest_level, id, name ),
        }
    }
}


/** Reads first byte of an event from `file` and returns the event type and first byte.
 *
 * # Effects
 *
 * `file` is advanced by one byte.
 */
fn get_event_type( file: &mut impl Read ) -> io::Result<(Option<EventType>, u8)> {

    let mut first_byte: [u8; 1] = [0; 1];

    match file.read(&mut first_byte) {
        Err( why ) => return Err( why ),
        Ok( byte_count ) => {
            // EOF
            if byte_count == 0 {
                return Ok(( None, 0 ))
            }
        }
    }

    match first_byte[0] {
        0..=0b01111111          => Ok(( Some(EventType::Timepoint), first_byte[0])), // timepoint (first bit 0)
        0b11111101              => Ok(( Some(EventType::HighPrecisionTime), first_byte[0])), // high precision time (0xFD)
        0b10100000..=0b10101111 => Ok(( Some(EventType::Data), first_byte[0])), // data (first nibble 0xA)
        0b11111110              => Ok(( Some(EventType::RegisterTimepoint), first_byte[0])), // timepoint registration (0xFE)
        0b11111111              => Ok(( Some(EventType::Padding), first_byte[0])), // padding byte (0xFF)
        _                       => Err( io::Error::new( io::ErrorKind::InvalidData, format!("invalid event type: {}", first_byte[0])  ) ) // ERROR: unknown (unsupported/unimplemented event type)
    }
}

/** Data types stored in a data event.
 *
 * Depending on data type, a data event may have different format.
 */
enum DataEventType {
    U8,
    U16,
    U32,
    U64,
    F32,
    F64,
    Str
}


/** Returns type of a data event, determined from the first byte of that event.
 *
 * Returns `None` if the event type is invalid (error in the stream)
 */
fn get_data_event_type( first_byte: u8 ) -> Option<DataEventType> {
    match first_byte & 0x0F {
        0x1       => Some(DataEventType::U8),
        0x2       => Some(DataEventType::U16),
        0x3       => Some(DataEventType::U32),
        0x4       => Some(DataEventType::U64),
        0x5       => Some(DataEventType::F32),
        0x6       => Some(DataEventType::F64),
        0x7..=0xE => None,
        0xF       => Some(DataEventType::Str),
        _         => None, // cannot happen since we clear the first nibble
    }
}

/** Return a single event read from `file`, **assuming its first byte has already been read**.
 *
 * # Arguments
 *
 * * `file`       - 'File' or stream we're reading from.
 * * `event_type` - Type of the event to read.
 * * `first_byte` - First byte of the event, read by the caller.
 *
 * # Errors
 *
 * * `io::Error(io::ErrorKind::InvalidData)` - if a data event has invalid type, or if a timepoint
 *   ID in a RegisterTimepoint event is invalid.
 * * `io::Error(any)` - on a file read error.
 */
fn read_event( file: &mut impl Read, event_type: EventType, first_byte: u8 )
    -> io::Result<Event> {
    match event_type {
        EventType::Timepoint => {
            // no conversion to real time here, just be raw event parsing with time deltas.
            let mut second_byte: [u8; 1] = [0; 1];
            file.read(&mut second_byte)?;
            let is_2byte = (second_byte[0] & 0b10000000) == 0;
            if is_2byte {
                let id         = first_byte;
                let time_delta = second_byte[0];
                return Ok( Event::Timepoint16{ id, time_delta } );
            }

            let mut bytes_23: [u8; 2] = [0; 2];
            file.read(&mut bytes_23)?;
            let is_4byte = (bytes_23[0] & 0b10000000) == 0;
            if is_4byte {
                let id: u16         = ((first_byte as u16) << 7) | ((second_byte[0] as u16) & 0b01111111);
                let time_delta: u16 = u16::from_be_bytes( bytes_23 );//((bytes_23[0] as u16) << 8) | (bytes_23[1] as u16);
                return Ok( Event::Timepoint32{ id, time_delta } );
            }
            else {
                let mut bytes_4567: [u8; 4] = [0; 4];
                file.read(&mut bytes_4567)?;

                let id: u16         = ((first_byte as u16) << 7) | ((second_byte[0] as u16) & 0b01111111);
                let time_delta: u64 = ((u16::from_be_bytes(bytes_23) as u64) << 32) | (u32::from_be_bytes(bytes_4567) as u64); // (bytes_23[0] << 40) | (bytes_23[1] << 32) | (bytes_4567[0] << 24) | (bytes_4567[1] << 16) | (bytes_4567[2] << 8) | bytes_4567[3];
                return Ok( Event::Timepoint64{ id, time_delta } );
            }
        },
        EventType::HighPrecisionTime => {
            let mut bytes_time: [u8; 7] = [0; 7];
            file.read(&mut bytes_time)?;
            let current_time: u64 = ((bytes_time[0] as u64) << 48) | ((u16::from_be_bytes(bytes_time[1 .. 3].try_into().unwrap()) as u64) << 32) | (u32::from_be_bytes(bytes_time[3 .. 7].try_into().unwrap()) as u64); // (bytes_time[0] << 48) | (bytes_time[1] << 40) | (bytes_time[2] << 32) | (bytes_time[3] << 24) | (bytes_time[4] << 16) | (bytes_time[5] << 8) | bytes_time[6];
            Ok(Event::HighPrecisionTime{ current_time })
        },
        EventType::Data => {
            let data_type = get_data_event_type( first_byte );
            match data_type {
                Some(t) => match t {
                    DataEventType::U8 => {
                        let mut bytes_id: [u8; 2] = [0; 2];
                        file.read(&mut bytes_id)?;
                        let id = (((bytes_id[0] & 0b00111111) as u16) << 8) | bytes_id[1] as u16;
                        let mut byte_u8: [u8; 1] = [0; 1];
                        file.read(&mut byte_u8)?;
                        Ok( Event::DataU8{ id: id,  value: byte_u8[0] } )
                    }
                    DataEventType::U16 => {
                        let mut bytes_id: [u8; 2] = [0; 2];
                        file.read(&mut bytes_id)?;
                        let id = (((bytes_id[0] & 0b00111111) as u16) << 8) | bytes_id[1] as u16;
                        let mut short: u16 = 0;
                        unsafe {
                            let mut short_slice = slice::from_raw_parts_mut(&mut short as *mut _ as *mut u8, 2);
                            file.read(&mut short_slice)?;
                        }
                        Ok( Event::DataU16{ id: id, value: short } )
                    }
                    DataEventType::U32 => {
                        let mut bytes_id: [u8; 2] = [0; 2];
                        file.read(&mut bytes_id)?;
                        let id = (((bytes_id[0] & 0b00111111) as u16) << 8) | bytes_id[1] as u16;
                        let mut int: u32 = 0;
                        unsafe {
                            let mut int_slice = slice::from_raw_parts_mut(&mut int as *mut _ as *mut u8, 4);
                            file.read(&mut int_slice)?;
                        }
                        Ok( Event::DataU32{ id: id, value: int })
                    }
                    DataEventType::U64 => {
                        let mut bytes_id: [u8; 2] = [0; 2];
                        file.read(&mut bytes_id)?;
                        let id = (((bytes_id[0] & 0b00111111) as u16) << 8) | bytes_id[1] as u16;
                        let mut long: u64 = 0;
                        unsafe {
                            let mut long_slice = slice::from_raw_parts_mut(&mut long as *mut _ as *mut u8, 8);
                            file.read(&mut long_slice)?;
                        }
                        Ok( Event::DataU64{ id: id, value: long })
                    }
                    DataEventType::F32 => {
                        let mut bytes_id: [u8; 2] = [0; 2];
                        file.read(&mut bytes_id)?;
                        let id = (((bytes_id[0] & 0b00111111) as u16) << 8) | bytes_id[1] as u16;
                        let mut float: f32 = 0.0;
                        unsafe {
                            let mut float_slice = slice::from_raw_parts_mut(&mut float as *mut _ as *mut u8, 4);
                            file.read(&mut float_slice)?;
                        }
                        Ok( Event::DataF32{ id: id, value: float } )
                    }
                    DataEventType::F64 => {
                        let mut bytes_id: [u8; 2] = [0; 2];
                        file.read(&mut bytes_id)?;
                        let id = (((bytes_id[0] & 0b00111111) as u16) << 8) | bytes_id[1] as u16;
                        let mut double: f64 = 0.0;
                        unsafe {
                            let mut double_slice = slice::from_raw_parts_mut(&mut double as *mut _ as *mut u8, 8);
                            file.read(&mut double_slice)?;
                        }
                        Ok( Event::DataF64{ id: id, value: double } )
                    }
                    DataEventType::Str => {
                        let mut bytes_id: [u8; 2] = [0; 2];
                        file.read(&mut bytes_id)?;
                        let id = (((bytes_id[0] & 0b00111111) as u16) << 8) | bytes_id[1] as u16;
                        let mut bytes = Vec::new();
                        loop {
                            let mut byte: [u8; 1] = [0; 1];
                            file.read(&mut byte)?;
                            if byte[0] == 0 {
                                break;
                            }
                            bytes.push(byte[0])
                        }
                        Ok( Event::DataStr{ id: id, value: String::from( String::from_utf8_lossy(&bytes) ) } )
                    }
                }
                None => Err( io::Error::new( io::ErrorKind::InvalidData, "invalid Data event type"  ) )
            }
        },
        EventType::RegisterTimepoint => {
            let mut bytes_register: [u8; 4] = [0; 4];
            file.read(&mut bytes_register)?;

            let event_id: u16 = u16::from_be_bytes( bytes_register[2 .. 4].try_into().unwrap() );

            let mut bytes = Vec::new();
            loop {
                let mut byte: [u8; 1] = [0; 1];
                file.read(&mut byte)?;
                if byte[0] == 0 {
                    break;
                }
                bytes.push(byte[0])
            }
            let name = String::from( String::from_utf8_lossy(&bytes) );
            let id_type = match bytes_register[1] {
                0b00000001 => IDType::Timepoint,
                0b00000011 => IDType::DataU8,
                0b00000010 => IDType::DataU16,
                0b00000100 => IDType::DataU32,
                0b00000101 => IDType::DataU64,
                0b00000110 => IDType::DataF32,
                0b00000111 => IDType::DataF64,
                0b00001000 => IDType::DataStr,
                _          => {
                    return Err( io::Error::new( io::ErrorKind::InvalidData, format!("invalid ID type {} at registration", bytes_register[1]) ) );
                }
            };
            if (event_id & 0b1100000000000000) != 0 {
                Err( io::Error::new( io::ErrorKind::InvalidData, "invalid timepoint event ID at registration (must be 14bit)"  ) )
            }
            else {
                Ok(Event::RegisterTimepoint{ nest_level: bytes_register[0] as i8, id_type: id_type, id: event_id, name: name})
            }
        },
        EventType::Padding => Ok(Event::Padding)
    }
}

/** Read and return a full event from `file`.
 *
 * # Errors
 *
 * * `io::Error(io::ErrorKind::InvalidData)` - if a data event has invalid type.
 */
fn get_event( file: &mut impl Read ) -> io::Result<Option<Event>> {
    let (event_type, first_byte) = get_event_type( file )?;
    match event_type {
        Some(t) => Ok( Some( read_event( file, t, first_byte )? ) ),
        None => Ok( None ) // EOF
    }
}


fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let file_name = &args[1]; // XXX bounds checking/error return/print if not enough args

    eprintln!( "converting file '{0}'...", file_name );

    let mut file = File::open(&file_name)?;

    loop {
        match get_event( &mut file ) {
            Err( why ) => panic!("{:?}", why), // XXX proper handling of erroneous input instead of a panic
            Ok( maybe_event ) => match maybe_event {
                Some( event ) => {
                    println!( "{}", event.to_string() )
                }
                None => break
            }
        }
    }

    eprintln!( "done" );
    Ok(())
}
