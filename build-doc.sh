#!/bin/sh

# ubuntu: need to have texlive-latex-extra installed

doxygen Doxyfile
cd doc/latex
make
mv refman.pdf ../nanoprof.pdf
