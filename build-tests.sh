#!/bin/sh

gcc -Os -Wall -Wextra -g tests/main.c -Inanoprof -o test
gcc -O3 -Wall -Wextra -g tests/main.c -Inanoprof -o test-O3
gcc -O2 -Wall -Wextra -g tests/main.c -Inanoprof -o test-O2
gcc -g -Wall -Wextra tests/main.c -Inanoprof -o test-debug
